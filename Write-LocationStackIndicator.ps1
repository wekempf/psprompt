# 

<#
.Synopsis
   Writes a '+' for every location stack entry.
.EXAMPLE
   Write-LocationStackIndicator
.EXAMPLE
   Write-LocationStackIndicator -NoNewline
#>
function Write-LocationStackIndicator {
    [CmdletBinding()]
    [OutputType([int])]
    Param
    (
        # The background color.
        [Parameter(Mandatory=$false)]
        [ConsoleColor]
        $BackgroundColor = $PromptBackground,

        # The foreground color.
        [Parameter(Mandatory=$false)]
        [ConsoleColor]
        $ForegroundColor = (PromptColor),

        # Don't write a newline.
        [switch]
        $NoNewline
    )

    Begin {
        $stack = '+' * (Get-Location -Stack:$null).Count
        if ($stack) {
            Write-Host -ForegroundColor:$ForegroundColor -BackgroundColor:$BackgroundColor -NoNewline:$NoNewline $stack
        }
    }

    Process {
    }

    End {
    }
}
