$PSPrompt = New-Object PSObject @{
    NormalForegroundColor = [ConsoleColor]::Yellow
    AdminForegroundColor = [ConsoleColor]::Red
    BackgroundColor = $Host.UI.RawUI.BackgroundColor
}
$PSPrompt | Add-Member -MemberType ScriptProperty -Name IsAdmin {
    $identity  = [System.Security.Principal.WindowsIdentity]::GetCurrent()
    $principal = New-Object System.Security.Principal.WindowsPrincipal($identity)
    $principal.IsInRole([System.Security.Principal.WindowsBuiltInRole]::Administrator)
}
$PSPrompt | Add-Member -MemberType ScriptProperty -Name ForegroundColor {
    if ($this.IsAdmin) {
        $this.AdminForegroundColor
    } else {
        $this.NormalForegroundColor
    }
}
$PSPrompt | Add-Member -MemberType ScriptProperty -Name Location {
    Get-Location
}
$PSPrompt | Add-Member -MemberType ScriptProperty -Name ShortLocation {
    $location = Get-Location
    # Shorten the Home directory portion
    $location = $location -replace [RegEx]::Escape($Home), '~'
    # Remove prefix for UNC paths
    $location = $location -replace '^[^:]+::', ''
    # Make path shorter
    $location = $location -replace '\\(\.?)([^\\])[^\\]*(?=\\)','\$1$2'
    $location
}
$PSPrompt | Add-Member -MemberType ScriptProperty -Name NextHistoryId {
    $history = Get-History
    if ($history.Count -gt 0) {
        $lastId = $history[$history.Count - 1].Id
    }
    $lastId + 1
}
$PSPrompt | Add-Member -MemberType ScriptMethod -Name UpdateCurrentWorkingDirector {
    [IO.Directory]::SetCurrentDirectory((Convert-Path (Get-Location -PSProvider FileSystem)))
}
Export-ModuleMember -Variable PSPrompt
